use std::fs::File;
use std::io;
use std::mem;
use std::os::windows::io::AsRawHandle;

use winapi::ctypes::c_void;
use winapi::shared::winerror::ERROR_LOCK_VIOLATION;
use winapi::um::fileapi::{LockFileEx, UnlockFileEx};
use winapi::um::minwinbase::{LOCKFILE_EXCLUSIVE_LOCK, LOCKFILE_FAIL_IMMEDIATELY};

pub enum LockError {
    IOError(io::Error),
    WouldBlock,
}

pub fn lock(fs: &File) -> Result<(), io::Error> {
    _lock(fs, LOCKFILE_EXCLUSIVE_LOCK).map_err(|x| match x {
        LockError::IOError(x) => x,
        _ => unreachable!(),
    })
}

pub fn try_lock(fs: &File) -> Result<(), LockError> {
    _lock(fs, LOCKFILE_EXCLUSIVE_LOCK | LOCKFILE_FAIL_IMMEDIATELY)
}

pub fn unlock(fs: &File) -> Result<(), io::Error> {
    unsafe {
        let handle = fs.as_raw_handle() as *mut c_void;
        let mut overlapped = mem::zeroed();
        if UnlockFileEx(handle, 0, 10, 0, &mut overlapped) != 0 {
            Ok(())
        } else {
            Err(io::Error::last_os_error())
        }
    }
}

fn _lock(fs: &File, flags: u32) -> Result<(), LockError> {
    unsafe {
        let handle = fs.as_raw_handle() as *mut c_void;
        let mut overlapped = mem::zeroed();
        if LockFileEx(handle, flags, 0, 10, 0, &mut overlapped) != 0 {
            Ok(())
        } else {
            let err = io::Error::last_os_error();
            if ERROR_LOCK_VIOLATION == err.raw_os_error().unwrap() as u32 {
                Err(LockError::WouldBlock)
            } else {
                Err(LockError::IOError(err))
            }
        }
    }
}
