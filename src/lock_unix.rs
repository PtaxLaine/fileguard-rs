use std::fs::File;
use std::io;
use std::os::unix::io::AsRawFd;

use libc::{flock, EWOULDBLOCK, LOCK_EX, LOCK_NB, LOCK_UN};

pub enum LockError {
    IOError(io::Error),
    WouldBlock,
}

pub fn lock(fs: &File) -> Result<(), io::Error> {
    _lock(fs, LOCK_EX).map_err(|x| match x {
        LockError::IOError(x) => x,
        _ => unreachable!(),
    })
}

pub fn try_lock(fs: &File) -> Result<(), LockError> {
    _lock(fs, LOCK_EX | LOCK_NB)
}

pub fn unlock(fs: &File) -> Result<(), io::Error> {
    _lock(fs, LOCK_UN).map_err(|x| match x {
        LockError::IOError(x) => x,
        _ => unreachable!(),
    })
}

fn _lock(fs: &File, flags: i32) -> Result<(), LockError> {
    unsafe {
        let fd = fs.as_raw_fd();
        if flock(fd, flags) == 0 {
            Ok(())
        } else {
            let err = io::Error::last_os_error();
            if EWOULDBLOCK == err.raw_os_error().unwrap() {
                Err(LockError::WouldBlock)
            } else {
                Err(LockError::IOError(err))
            }
        }
    }
}
