use std::fs::{File, OpenOptions};
use std::io;
use std::path::Path;

#[cfg(windows)]
#[path = "lock_win.rs"]
mod lock;

#[cfg(unix)]
#[path = "lock_unix.rs"]
mod lock;

#[derive(Debug)]
pub struct FileGuard {
    fs: File,
}

impl FileGuard {
    pub fn lock<P: AsRef<Path>>(file: P) -> Result<Self, io::Error> {
        let fs = Self::open(file)?;
        lock::lock(&fs)?;
        Ok(Self { fs })
    }

    pub fn try_lock<P: AsRef<Path>>(file: P) -> Result<Option<Self>, io::Error> {
        let fs = Self::open(file)?;
        if let Err(err) = lock::try_lock(&fs) {
            match err {
                lock::LockError::IOError(x) => return Err(x),
                lock::LockError::WouldBlock => return Ok(None),
            }
        }
        Ok(Some(Self { fs }))
    }

    pub fn guard<F: FnOnce() -> T, T>(&self, func: F) -> T {
        func()
    }

    fn open<P: AsRef<Path>>(file: P) -> Result<File, io::Error> {
        OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(file)
    }
}

impl Drop for FileGuard {
    fn drop(&mut self) {
        let _ = lock::unlock(&self.fs);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn lock1() {
        let lock1 = FileGuard::lock("./target/test_1_lock").unwrap();
        assert_eq!(lock1.guard(|| 71236), 71236);
        drop(lock1);
        let lock2 = FileGuard::lock("./target/test_1_lock").unwrap();
        assert_eq!(lock2.guard(|| 78941), 78941);
    }

    #[test]
    fn try_lock() {
        let lock1 = FileGuard::try_lock("./target/test_2_lock").unwrap();
        assert!(lock1.is_some());
        assert_eq!(lock1.as_ref().unwrap().guard(|| 789213), 789213);
        drop(lock1);
        let lock2 = FileGuard::try_lock("./target/test_2_lock").unwrap();
        assert!(lock2.is_some());
        assert_eq!(lock2.as_ref().unwrap().guard(|| 4221), 4221);
    }
}
